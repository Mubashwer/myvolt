﻿using System.ComponentModel.DataAnnotations;

namespace MyVolt.Models
{
    public class User
    {
        public int Id { get; set; }
        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        public string RFIDSerial { get; set; }
        [Required]
        public decimal Balance { get; set; }
    }
}